package com.testtask.travelplanner.controller;

import com.testtask.travelplanner.model.ForecastRecord;
import com.testtask.travelplanner.model.Itinerary;
import com.testtask.travelplanner.repository.ForecastRepository;
import com.testtask.travelplanner.repository.ItineraryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class ItineraryController {
    ForecastRepository forecastRepository;
    ItineraryRepository itineraryRepository;

    public ItineraryController(ForecastRepository forecastRepository, ItineraryRepository itineraryRepository) {
        this.forecastRepository = forecastRepository;
        this.itineraryRepository = itineraryRepository;
    }

    @PostMapping("/saveItinerary")
    public String saveItinerary(@RequestParam String itineraryName, Model model){
        List<ForecastRecord> forecastRecords = forecastRepository.findAll();

        if (forecastRecords.isEmpty()) {
            assignDefaultModelAttributes(model);
            model.addAttribute("emptyTable", true);
            model.addAttribute("NoRecordsToMakeItinerary", true);
            model.addAttribute("jsonParsingStatus", true); // not to display previous message
            return "index";
        }

        for (ForecastRecord forecastRecord: forecastRecords) {
            Itinerary itinerary = new Itinerary(itineraryName,
                    forecastRecord.getCreatedAt(),
                    forecastRecord.getDay(),
                    forecastRecord.getTime(),
                    forecastRecord.getCityName(),
                    forecastRecord.getCountryCode(),
                    forecastRecord.getTemperature(),
                    forecastRecord.getCloudiness(),
                    forecastRecord.rain,
                    forecastRecord.snow);

            itineraryRepository.save(itinerary);
        }

        forecastRepository.deleteAll();

        assignDefaultModelAttributes(model);
        model.addAttribute("emptyTable", forecastRepository.count() == 0);
        model.addAttribute("itinerarySaved", true);
        model.addAttribute("jsonParsingStatus", true); // not to display previous message

        return "index";
    }

    @GetMapping("/showAllItineraries")
    public String showAllItineraries(Model model){
        assignDefaultModelAttributes(model);

        if (itineraryRepository.count() == 0) {
            model.addAttribute("emptyItineraryTable", itineraryRepository.count() == 0);
        } else {
            assignItineraryNames(model);
        }

        model.addAttribute("itinerariesList", itineraryRepository.findAll());

        return "itineraries";
    }

    @GetMapping("/filterItineraries")
    public String filterItineraries(@RequestParam String name, Model model) {

        assignDefaultModelAttributes(model);
        assignItineraryNames(model);

        if (name.equals("showAll")) {
            model.addAttribute("itinerariesList", itineraryRepository.findAll());
            model.addAttribute("filteredItineraryDisplayed", false);
        } else {
            List<Itinerary> selectedItinerary = itineraryRepository.findAll().stream()
                    .filter(x -> x.getItineraryName().equals(name))
                    .collect(Collectors.toList());

            model.addAttribute("itinerariesList", selectedItinerary);
            model.addAttribute("filteredItineraryDisplayed", true);
        }

        return "itineraries";
    }

    @GetMapping("/startNewItinerary")
    public String startNewItinerary(){

        return "index";
    }

    private void assignDefaultModelAttributes(Model model) {
        model.addAttribute("title", "Travel Planner");
        model.addAttribute("minDate", new SimpleDateFormat("yyyy-MM-dd").format(Date.from(Instant.now())));
        model.addAttribute("maxDate",
                new SimpleDateFormat("yyyy-MM-dd").format(Date.from(Instant.now().plus(4, ChronoUnit.DAYS))));
    }

    private void assignItineraryNames(Model model) {
        Set<String> itineraryNames = itineraryRepository.findAll().stream()
                .map(Itinerary::getItineraryName)
                .collect(Collectors.toSet());
        model.addAttribute("itineraryNames", itineraryNames);
    }

}
