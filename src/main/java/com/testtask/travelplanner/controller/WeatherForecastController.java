package com.testtask.travelplanner.controller;

import com.testtask.travelplanner.model.ForecastRecord;
import com.testtask.travelplanner.repository.ForecastRepository;
import com.testtask.travelplanner.repository.ItineraryRepository;
import com.testtask.travelplanner.service.ForecastService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class WeatherForecastController {
    ForecastService forecastService;
    ForecastRepository forecastRepository;
    ItineraryRepository itineraryRepository;

    public WeatherForecastController(ForecastService forecastService, ForecastRepository forecastRepository, ItineraryRepository itineraryRepository) {
        this.forecastService = forecastService;
        this.forecastRepository = forecastRepository;
        this.itineraryRepository = itineraryRepository;
    }

    @GetMapping("/")
    public String homePage(Model model) {

        assignDefaultModelAttributes(model);
        model.addAttribute("firstVisit",
                forecastRepository.count() == 0 && itineraryRepository.count() == 0);
        if (forecastRepository.count() == 0) {
            model.addAttribute("emptyTable", forecastRepository.count() == 0);
            model.addAttribute("jsonParsingStatus", true); // not to display previous message
        } else {
            model.addAttribute("jsonParsingStatus", true); // not to display previous message
            assignCitiesAdded(model);
            model.addAttribute("forecastRecords", forecastRepository.findAll());
        }

        return "index";
    }

    @PostMapping("/addForecastRecord")
    public String addForecastRecord(@RequestParam String city, @RequestParam String day, Model model) {

        List<ForecastRecord> forecastRecords = forecastService.getForecastData(city, day);

        for (ForecastRecord record : forecastRecords) {
            forecastRepository.save(record);
        }

        assignDefaultModelAttributes(model);

        model.addAttribute("jsonParsingStatus", forecastService.getJsonParsingStatus());

        assignCitiesAdded(model);

        model.addAttribute("forecastRecords", forecastRepository.findAll());

        model.addAttribute("firstVisit",
                forecastRepository.count() == 0 && itineraryRepository.count() == 0);
        model.addAttribute("emptyTable", forecastRepository.count() == 0);

        return "index";
    }

    @GetMapping("/filterForecastRecords")
    public String filterForecastRecords(@RequestParam String city, Model model) {

        assignDefaultModelAttributes(model);

        assignCitiesAdded(model);

        model.addAttribute("jsonParsingStatus", true); // not to display previous message

        if (city.equals("showAll")) {
            model.addAttribute("forecastRecords", forecastRepository.findAll());
            model.addAttribute("filteredResultDisplayed", false);
        } else {
            List<ForecastRecord> records = forecastRepository.findAll().stream()
                    .filter(x -> x.getCityName().equals(city))
                    .collect(Collectors.toList());

            model.addAttribute("forecastRecords", records);
            model.addAttribute("filteredResultDisplayed", true);
        }

        return "index";
    }

    private void assignDefaultModelAttributes(Model model) {
        model.addAttribute("title", "Travel Planner");
        model.addAttribute("minDate", new SimpleDateFormat("yyyy-MM-dd").format(Date.from(Instant.now())));
        model.addAttribute("maxDate",
                new SimpleDateFormat("yyyy-MM-dd").format(Date.from(Instant.now().plus(4, ChronoUnit.DAYS))));
    }

    private void assignCitiesAdded(Model model) {
        Set<String> citiesAdded = forecastRepository.findAll().stream()
                .map(ForecastRecord::getCityName)
                .collect(Collectors.toSet());
        model.addAttribute("citiesAdded", citiesAdded);
    }
}
