package com.testtask.travelplanner.repository;

import com.testtask.travelplanner.model.ForecastRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Repository
public interface ForecastRepository extends JpaRepository<ForecastRecord, Long> {
    List<ForecastRecord> findAll();

//    @Query("select fr from ForecastRecord fr where fr.createdAt <= :createdAt")
//    List<ForecastRecord> findAllWithCreationDateTimeBefore(@Param("createdAt") Date createdAt);
}
