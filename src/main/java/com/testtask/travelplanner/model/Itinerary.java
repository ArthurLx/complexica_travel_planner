package com.testtask.travelplanner.model;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Getter
@ToString
public class Itinerary {
    @Id
    @GeneratedValue
    public Long id;
    public String itineraryName;
    public Instant createdAt;
    public String day;
    public String time;
    public String cityName;
    public String countryCode;
    public String temperature;
    public String cloudiness;
    public boolean rain;
    public boolean snow;

    public Itinerary() {
    }

    public Itinerary(String itineraryName, Instant createdAt, String day, String time, String cityName, String countryCode, String temperature, String cloudiness, boolean rain, boolean snow) {
        this.itineraryName = itineraryName;
        this.createdAt = createdAt;
        this.day = day;
        this.time = time;
        this.cityName = cityName;
        this.countryCode = countryCode;
        this.temperature = temperature;
        this.cloudiness = cloudiness;
        this.rain = rain;
        this.snow = snow;
    }
}
