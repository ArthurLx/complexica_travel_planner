package com.testtask.travelplanner.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.testtask.travelplanner.model.ForecastRecord;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ForecastService {
    private boolean jsonParsed;

    public List<ForecastRecord> getForecastData(String city, String day) {

        String forecastJsonString = getForecastJsonString(city);

        List<ForecastRecord> forecastRecordList = parseForecastJsonString(city, day, forecastJsonString);

        List<ForecastRecord> filteredForecastRecordList = forecastRecordList.stream()
                .filter(x -> LocalDate.parse(x.getDay()).compareTo(LocalDate.parse(day)) == 0)
                .filter(x -> x.getTime().startsWith("12")
                        || x.getTime().startsWith("15")
                        || x.getTime().startsWith("18"))
                .collect(Collectors.toList());

        return filteredForecastRecordList;

    }

    public String jsonParsingStatusDesc() {

        return "";
    }

    private String getForecastJsonString(String city) {

        String API_KEY = "dd61d47ec80cd35dfa22f8a4c56afb80";
        String UNITS = "metric";
        String urlString = "http://api.openweathermap.org/data/2.5/forecast?q=" + city
                + "&units=" + UNITS + "&appid=" + API_KEY;
        StringBuilder forecastJsonString = new StringBuilder();

        try {
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;

            while((line = buffReader.readLine()) != null) {
                forecastJsonString.append(line);
            }

            buffReader.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return forecastJsonString.toString();
    }

    private List<ForecastRecord> parseForecastJsonString(String city, String day, String forecastJsonString) {

        ObjectMapper mapper = new ObjectMapper();
        List<ForecastRecord> forecastRecordList = new ArrayList<>();

        try {
            ArrayNode listArrayNode = (ArrayNode) mapper.readTree(forecastJsonString).get("list");

            JsonNode cityNode = mapper.readTree(forecastJsonString.toString()).get("city");
            String countryCode = cityNode.get("country").toString();

            for(JsonNode itemNode : listArrayNode) {

                int unix_timestamp = Integer.parseInt(itemNode.get("dt").toString());
                Instant instant = Instant.ofEpochSecond(unix_timestamp);
                String dayParsed = instant.toString().split("[TZ]")[0];
                String timeParsed = instant.toString().split("[TZ]")[1];

//                LocalDate dateParsed = LocalDate.parse(dayParsed);    // either this or filter
//                LocalDate dayPassed = LocalDate.parse(day);
//                if (dayPassed.compareTo(dateParsed) != 0){ continue; }
//                if (!(timeParsed.startsWith("12") || timeParsed.startsWith("15") || timeParsed.startsWith("18"))) { continue; }

                String temperature = itemNode.get("main").get("temp").toString();
                String cloudiness = itemNode.get("clouds").get("all").toString();
                boolean rain = itemNode.has("rain");
                boolean snow = itemNode.has("snow");

                forecastRecordList.add(
                        new ForecastRecord(Instant.now(), dayParsed, timeParsed, city, countryCode, temperature, cloudiness, rain, snow));

            }
            jsonParsed = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            jsonParsed = false;
        }

        return forecastRecordList;
    }

    public boolean getJsonParsingStatus() {
        return jsonParsed;
    }

//    public void resetJsonParsed() {
//        jsonParsed = true;
//    }
}
