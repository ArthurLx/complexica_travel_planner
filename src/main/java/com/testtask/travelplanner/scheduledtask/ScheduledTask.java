package com.testtask.travelplanner.scheduledtask;

import com.testtask.travelplanner.model.ForecastRecord;
import com.testtask.travelplanner.repository.ForecastRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class ScheduledTask {

    ForecastRepository forecastRepository;

    private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);

    public ScheduledTask(ForecastRepository forecastRepository) {
        this.forecastRepository = forecastRepository;
    }

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.HOURS)
    public void clearDbCache() {

        List<ForecastRecord> selectAll = forecastRepository.findAll();

        List<ForecastRecord> temporarList = selectAll.stream()
                .filter(x ->
                        x.getCreatedAt().compareTo((Instant.now().minus(1, ChronoUnit.HOURS))) <= -1)
                .collect(Collectors.toList());

        if(temporarList.size() > 0) {
            log.info("Found {} rows", temporarList.size());

            for (ForecastRecord forecastRecord : temporarList) {
                forecastRepository.delete(forecastRecord);
            }

            log.info("Database cache cleared, {} rows touched", temporarList.size());
        }
        log.info("Waiting...");
    }
}
