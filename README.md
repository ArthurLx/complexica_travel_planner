## Project Information:

A spring boot application that provides weather forecast information for a specific city and day.  
For a backend is responsible Java Spring, H2 in-memory database keeps all user's data within one hour.  
Frontend implemented using mustache template engine.